# PyStatOpics

A Python package for data analysis and visualization in modern optics, including refraction and reflection, interference, diffraction, and study of the polarization of light. 